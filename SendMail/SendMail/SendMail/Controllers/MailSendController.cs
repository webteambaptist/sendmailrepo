﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using NLog;
using SendMail.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SendMail.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MailSendController : ControllerBase
    {
        private static string _environment;
        private static string _relay;
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public MailSendController(IConfiguration c)
        {
            _environment = c["AppSettings:environment"];
            _relay = c["AppSettings:RELAY"];
        }

        public string Get()
        {
            Logger.Info("Send Mail Microservice Started.....");
            return "Send Mail Microservice Started.....";
        }

        [HttpPost]
        [Route("sendMailService")]
        public ActionResult SendMailService([FromBody] Mail message)
        {
            var smtpClient = new SmtpClient { DeliveryMethod = SmtpDeliveryMethod.Network };
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = "email.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                var m = new MailMessage(message.From, message.To) {Body = message.Body};


                Logger.Info("Sending Email(SendMail.SendMail.SendMailService())....");
                m.Subject = _environment + " :: " + message.Subject;
                m.IsBodyHtml = message.IsBodyHtml;

                smtpClient.Host = _relay;
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;

                if (string.IsNullOrEmpty(m.To.ToString())) 
                    return BadRequest();
                try
                {
                    smtpClient.Send(m);
                    return Ok();
                }
                catch (Exception ex)
                {
                    Logger.Error("smtpClient.Send FAILED " + ex.Message);
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("SendMail/sendMail " + ex.Message);
                return BadRequest();
            }
        }
    }
}
